tryton-modules-country (7.0.0-4) unstable; urgency=medium

  * Update the Depends for 7.0.
  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:55:39 +0200

tryton-modules-country (7.0.0-3) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:22 +0200

tryton-modules-country (7.0.0-2) unstable; urgency=medium

  * Remove superseeded patches, update remaining patches.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Oct 2024 17:58:49 +0200

tryton-modules-country (7.0.0-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Switch to pgpmode=none in the watch file.
  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083864).
  * Merging upstream version 7.0.0.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Oct 2024 20:00:54 +0200

tryton-modules-country (6.0.3-2) sid; urgency=medium

  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 13 Feb 2023 12:57:49 +0100

tryton-modules-country (6.0.3-1) unstable; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Merging upstream version 6.0.3.
  * Refresh patches.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 18 Oct 2022 10:16:31 +0200

tryton-modules-country (6.0.2-2) unstable; urgency=medium

  * Adapt for recent pycountry/isocodes versions (#1002073).
  * Add 03-manage-unkwon-subdivision-type.patch.
  * Add 04-manage-unavailable-codes.patch.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 08 Jun 2022 20:36:05 +0200

tryton-modules-country (6.0.2-1) unstable; urgency=medium

  * Merging upstream version 6.0.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 10 Mar 2022 09:45:48 +0100

tryton-modules-country (6.0.1-4) unstable; urgency=medium

  * Disable the test for the country import script (Closes: #1002073).
    The module works without iso-codes, it is just the import script that is
    not adapted for iso-codes 4.8.0.
    Patch to be removed once https://bugs.tryton.org/issue11078 is solved.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 22 Dec 2021 09:28:14 +0100

tryton-modules-country (6.0.1-3) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:52:56 +0100

tryton-modules-country (6.0.1-2) unstable; urgency=medium

  * Add test depends for import scripts.
  * Update Recommends with the script requirements.
  * Add 01-disable_import_postal_codes.patch.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 27 Oct 2021 10:16:17 +0200

tryton-modules-country (6.0.1-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.1.
  * Use same debhelper compat as for server and client.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:12:37 +0200

tryton-modules-country (5.0.3-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 10:52:32 +0200

tryton-modules-country (5.0.2-1) unstable; urgency=medium

  * Add the correct license for the distributed icons.
  * Merging upstream version 5.0.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 12:57:25 +0200

tryton-modules-country (5.0.1-2) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:16:59 +0200

tryton-modules-country (5.0.1-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 10:33:01 +0100

tryton-modules-country (5.0.0-2) unstable; urgency=medium

  * Cleanup white space.
  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:35:09 +0100

tryton-modules-country (5.0.0-1) unstable; urgency=medium

  * Merging upstream version 5.0.0.
  * Updating copyright file.
  * Updating to Standards-Version: 4.2.1, no changes needed.
  * Update signing-key.asc with the minimized actual upstream maintainer
    key.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 20 Nov 2018 17:08:04 +0100

tryton-modules-country (4.6.0-2) unstable; urgency=medium

  * Add python-urllib3 to Recommends.
  * Changing to correct Python3 Recommend python3-urllib3.
  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 29 Mar 2018 21:14:46 +0200

tryton-modules-country (4.6.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Merging upstream version 4.6.0.
  * Use https in the watch file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 07 Nov 2017 10:20:08 +0100

tryton-modules-country (4.4.0-4) unstable; urgency=medium

  * Switch to Python3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Aug 2017 12:35:25 +0200

tryton-modules-country (4.4.0-3) unstable; urgency=medium

  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 16:02:45 +0200

tryton-modules-country (4.4.0-2) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Jul 2017 12:36:08 +0200

tryton-modules-country (4.4.0-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 4.4.0.
  * Updating debian/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 10 Jun 2017 23:29:43 +0200

tryton-modules-country (4.2.0-1) unstable; urgency=medium

  * Updating to Standards-Version: 3.9.8, no changes needed.
  * Merging upstream version 4.2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 05 Dec 2016 15:31:18 +0100

tryton-modules-country (4.0.1-1) unstable; urgency=medium

  * Updating signing-key.asc with the actual upstream maintainer keys.
  * Merging upstream version 4.0.0.
  * Merging upstream version 4.0.1.
  * Updating the copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 30 May 2016 19:28:46 +0200

tryton-modules-country (3.8.0-2) unstable; urgency=medium

  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.
  * Disable tests globally for all Python versions.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 13:08:51 +0100

tryton-modules-country (3.8.0-1) unstable; urgency=medium

  * Updating year of debian copyright.
  * Adapting section naming in gbp.conf to current git-buildpackage.
  * Improving description why we can not run the module test suites.
  * Merging upstream version 3.8.0.
  * Adding a man page for trytond_import_zip.
  * Adding a lintian-override for trytond_import_zip.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 13 Nov 2015 14:00:25 +0100

tryton-modules-country (3.6.0-1) unstable; urgency=medium

  * Wrapping and sorting control files (wrap-and-sort -bts).
  * Merging upstream version 3.6.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 26 Apr 2015 23:49:17 +0200

tryton-modules-country (3.4.1-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Improving boilerplate in d/control (Closes: #771722).
  * Merging upstream version 3.4.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Mar 2015 12:39:15 +0100

tryton-modules-country (3.4.0-1) unstable; urgency=medium

  * Updating signing key while using now plain .asc files instead of .pgp
    binaries.
  * Adding actual upstream signing key.
  * Updating to Standards-Version: 3.9.6, no changes needed.
  * Merging upstream version 3.4.0.
  * Adding tryton-proteus to Recommends.
  * Installing scripts to docs.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 21 Oct 2014 23:55:51 +0200

tryton-modules-country (3.2.0-1) unstable; urgency=medium

  * Removing  LC_ALL=C.UTF-8 as build environment.
  * Merging upstream version 3.2.0.
  * Updating copyright.
  * Bumping minimal required Python version to 2.7.
  * Updating gbp.conf for usage of upstream tarball compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 24 Apr 2014 15:27:17 +0200

tryton-modules-country (3.0.0-3) unstable; urgency=medium

  * Updating year in debian copyright.
  * Removing debian/source/options, we are building with dpkg defaults.
  * Removing PYBUILD_DESTDIR_python2 from rules, it is no more needed.
  * Adding pgp verification for uscan.
  * Adding gbp.conf for usage with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Mar 2014 17:15:03 +0100

tryton-modules-country (3.0.0-2) unstable; urgency=low

  * Pointing VCS fields to new location on alioth.debian.org.
  * Using dpkg defaults for xz compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 02 Dec 2013 21:16:12 +0100

tryton-modules-country (3.0.0-1) unstable; urgency=low

  * Merging upstream version 3.0.0.
  * Updating to standards version 3.9.5, no changes needed.
  * Changing to buildsystem pybuild.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 25 Nov 2013 17:54:01 +0100

tryton-modules-country (2.8.0-3) unstable; urgency=low

  * Adapting the rules file to work also with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 06 Aug 2013 13:32:49 +0200

tryton-modules-country (2.8.0-2) unstable; urgency=low

  * Adding doc/ to docs file.
  * Simplifying package layout by renaming <pkg_name>.docs to docs.
  * Removing needless empty line in rules.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 31 May 2013 17:26:27 +0200

tryton-modules-country (2.8.0-1) experimental; urgency=low

  * Merging upstream version 2.8.0.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 02 May 2013 15:20:05 +0200

tryton-modules-country (2.6.0-4) experimental; urgency=low

  * Removing Daniel from Uploaders. Thanks for your work! (Closes: #704379).
  * Improving update of major version in Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 27 Apr 2013 15:06:57 +0200

tryton-modules-country (2.6.0-3) experimental; urgency=low

  * Updating Vcs-Git to correct address.
  * Adding watch file. Thanks to Bart Martens <bartm@debian.org>.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 23 Mar 2013 14:00:35 +0100

tryton-modules-country (2.6.0-2) experimental; urgency=low

  * Removing obsolete Dm-Upload-Allowed
  * Updating to Standards-Version: 3.9.4, no changes needed.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 16 Feb 2013 21:38:26 +0100

tryton-modules-country (2.6.0-1) experimental; urgency=low

  * Merging upstream version 2.6.0.
  * Bumping versioned tryton depends to 2.6.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 24 Oct 2012 14:24:25 +0200

tryton-modules-country (2.4.0-2) experimental; urgency=low

  [ Daniel Baumann ]
  * Updating maintainers field.
  * Updating vcs fields.
  * Correcting copyright file to match format version 1.0.
  * Switching to xz compression.
  * Updating to debhelper version 9.

  [ Mathias Behrle ]
  * Merging branch debian-wheezy-2.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 18 Sep 2012 13:36:41 +0200

tryton-modules-country (2.4.0-1) experimental; urgency=low

  * Updating to Standards-Version: 3.9.3, no changes needed.
  * Updating year in copyright.
  * Adding myself to copyright.
  * Adding Format header for DEP5.
  * Merging upstream version 2.4.0.
  * Updating Copyright.
  * Bumping versioned tryton depends to 2.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 26 Apr 2012 19:29:41 +0200

tryton-modules-country (2.2.0-1) unstable; urgency=low

  * Bumping X-Python-Version to >=2.6.
  * Updating versioned tryton depends to 2.2.
  * Merging upstream version 2.2.0.
  * Removing deprecated XB-Python-Version for dh_python2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 26 Dec 2011 14:00:53 +0100

tryton-modules-country (2.0.0-2) unstable; urgency=low

  [ Daniel Baumann ]
  * Removing for new source package version obsoleted README.source file.
  * Adding options for source package.
  * Compacting copyright file.
  * Not wrapping uploaders field, it does not exceed 80 chars.

  [ Mathias Behrle ]
  * Moving from deprecated python-support to dh_python2, thanks to
    Charlie Smotherman <cjsmo@cableone.net> (Closes: #633713).

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 14 Jul 2011 01:14:11 +0200

tryton-modules-country (2.0.0-1) unstable; urgency=low

  * Updating to standards version 3.9.2.
  * Merging upstream version 2.0.0.
  * Updating Copyright.
  * Updating versioned tryton depends to 2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 24 May 2011 21:33:54 +0200

tryton-modules-country (1.8.0-2) unstable; urgency=low

  * Changing my email address.
  * Setting minimal Python version to 2.5.
  * Updating Copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 15 Feb 2011 13:09:26 +0100

tryton-modules-country (1.8.0-1) experimental; urgency=low

  * Updating standards version to 3.9.0.
  * Updating to debhelper version 8.
  * Updating to standards version 3.9.1.
  * Switching to source format 3.0 (quilt).
  * Merging upstream version 1.8.0.
  * Updating versioned tryton depends to 1.8.

 -- Daniel Baumann <daniel@debian.org>  Fri, 12 Nov 2010 12:59:15 +0100

tryton-modules-country (1.6.0-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Adding Dm-Upload-Allowed in control in preparation for Mathias.

  [ Mathias Behrle ]
  * Merging upstream version 1.6.0.
  * Updating copyright.
  * Updating depends.

 -- Mathias Behrle <mathiasb@mbsolutions.selfip.biz>  Mon, 10 May 2010 23:43:01 +0200

tryton-modules-country (1.4.0-2) unstable; urgency=low

  * Making depends versioned for tryton 1.4.
  * Adding explicit debian source version 1.0 until switch to 3.0.
  * Updating year in copyright file.
  * Removing unneeded python-all-dev from build-depends.
  * Updating to standards 3.8.4.
  * Updating README.source.

 -- Daniel Baumann <daniel@debian.org>  Sat, 20 Feb 2010 10:25:36 +0100

tryton-modules-country (1.4.0-1) unstable; urgency=low

  * Merging upstream version 1.4.0.

 -- Daniel Baumann <daniel@debian.org>  Mon, 19 Oct 2009 22:07:05 +0200

tryton-modules-country (1.2.1-4) unstable; urgency=low

  * Updating to standards version 3.8.3.
  * Adding maintainer homepage field to control.
  * Adding README.source.
  * Moving maintainer homepage field to copyright.
  * Updating README.source.
  * Using ascii only characters in copyright.

 -- Daniel Baumann <daniel@debian.org>  Sat, 05 Sep 2009 09:40:50 +0200

tryton-modules-country (1.2.1-3) unstable; urgency=low

  * Updating maintainer field.
  * Updating vcs fields.
  * Wrapping lines in control.

 -- Daniel Baumann <daniel@debian.org>  Mon, 10 Aug 2009 20:05:56 +0200

tryton-modules-country (1.2.1-2) unstable; urgency=low

  * Minimizing rules file.

 -- Daniel Baumann <daniel@debian.org>  Sun, 26 Jul 2009 20:33:08 +0200

tryton-modules-country (1.2.1-1) unstable; urgency=low

  * Updating package to standards version 3.8.2.
  * Merging upstream version 1.2.1.

 -- Daniel Baumann <daniel@debian.org>  Fri, 10 Jul 2009 14:15:25 +0200

tryton-modules-country (1.2.0-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Merging upstream version 1.2.0.
  * Tidy rules file.
  * Updating copyright file for new upstream release.

  [ Mathias Behrle ]
  * Updating application description.

  [ Daniel Baumann ]
  * Correcting wrapping of control file.

 -- Daniel Baumann <daniel@debian.org>  Tue, 21 Apr 2009 20:41:00 +0200

tryton-modules-country (1.0.2-2) unstable; urgency=low

  * Adding Mathias to uploaders.

 -- Daniel Baumann <daniel@debian.org>  Sat, 28 Mar 2009 16:27:00 +0100

tryton-modules-country (1.0.2-1) unstable; urgency=low

  * Merging upstream version 1.0.2.

 -- Daniel Baumann <daniel@debian.org>  Mon, 23 Mar 2009 07:04:00 +0100

tryton-modules-country (1.0.1-1) unstable; urgency=low

  * Merging upstream version 1.0.1.
  * Updating standards to 3.8.1.
  * Making package arch all as it should be (Closes: #520800).

 -- Daniel Baumann <daniel@debian.org>  Sun, 22 Mar 2009 23:14:00 +0100

tryton-modules-country (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #506095).

 -- Daniel Baumann <daniel@debian.org>  Mon, 12 Jan 2009 15:49:00 -0500
